import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  Image,
  FlatList,
  Modal,
  Pressable,
  Alert,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {Button, Card, Icon} from 'react-native-elements';
import axios from 'axios';

export default function HomeScreen({navigation}) {
  const [bookDataList, setBookDataList] = useState([]);
  const [twoColumnLayout, setTwoColumnLayout] = useState(false);
  const [sortModalVisible, setSortModalVisible] = useState(false);
  const [filterModalVisible, setFilterModalVisible] = useState(false);
  const [addBookModalVisible, setAddBookModalVisible] = useState(false);
  const [publisher, setPublisher] = useState('');
  const [author, setAuthor] = useState('');
  const [bookName, setBookName] = useState('');
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');

  const resetAddBookFormData = () => {
    setAuthor('');
    setPublisher('');
    setBookName('');
    setPrice('');
    setDescription('');
  };

  const Logout = () => {
    navigation.navigate('Onboarding');
  };

  const changeLayout = () => {
    if (twoColumnLayout == true) {
      setTwoColumnLayout(false);
    } else {
      setTwoColumnLayout(true);
    }
  };

  const getBookDataList = async () => {
    await axios
      .get('https://61dddb4af60e8f0017668ac5.mockapi.io/api/v1/Book')
      .then(res => setBookDataList(res.data))
      .catch(error => console.log(error));
  };

  const addBook = async () => {
    const createBookData = {
      publisher: {publisher},
      author: {author},
      name: {name},
      price: {price},
      description: {description},
      currency_code: '$',
      downloads: 17682,
    };
    await axios
      .post(
        'https://61dddb4af60e8f0017668ac5.mockapi.io/api/v1/Book',
        createBookData,
      )
      .then(res => setBookDataList(res.data), Alert.alert('New Book Added'))
      .catch(error => console.log(error));
    getBookDataList();
  };

  const renderItemTwoColumnLayout = ({item}) => (
    <View key={item.id}>
      <Card key={item.id} containerStyle={{borderWidth: 5}}>
        <View style={styles.twoColumnLayoutWrapper}>
          <Image source={require('../assets/Books.jpg')} style={styles.logo} />
          <View>
            <Text style={styles.bookNameStyle1}>{item.name}</Text>
            <Text>Author: {item.author}</Text>
            <Text>Downloads: {item.downloads}</Text>
          </View>
          <Text style={styles.priceStyle}>
            {item.currency_code}
            {' ' + item.price}
          </Text>
        </View>
      </Card>
    </View>
  );

  const renderItemOneColumnLayout = ({item}) => (
    <View key={item.id}>
      <Card
        key={item.id}
        style={{backgroundColor: ''}}
        containerStyle={{borderWidth: 5}}>
        <View style={styles.oneColumnLayoutWrapper}>
          <Image source={require('../assets/Books.jpg')} style={styles.logo} />
          <View>
            <Text style={styles.bookNameStyle2}>{item.name}</Text>
            <Text>Author: {item.author}</Text>
            <Text>Downloads: {item.downloads}</Text>
          </View>
          <Text style={styles.priceStyle}>
            {item.currency_code}
            {' ' + item.price}
          </Text>
        </View>
      </Card>
    </View>
  );

  useEffect(() => {
    getBookDataList();
  }, []);

  console.log('New 1 = ' + JSON.stringify(bookDataList));

  return (
    <>
      <View></View>
      <View style={styles.mainWrapper}>
        {bookDataList.length == 0 ? (
          <ActivityIndicator size="large" color="skyblue" />
        ) : twoColumnLayout ? (
          <FlatList
            key={'_'}
            contentContainerStyle={{
              alignItems: 'center',
            }}
            numColumns={2}
            data={bookDataList}
            keyExtractor={item => '_' + item.id}
            renderItem={renderItemTwoColumnLayout}
          />
        ) : (
          <FlatList
            key={'#'}
            data={bookDataList}
            keyExtractor={item => '#' + item.id}
            renderItem={renderItemOneColumnLayout}
          />
        )}
      </View>

      <View style={styles.utilButtons}>
        <View style={{width: 130}}>
          <Button
            buttonStyle={{
              backgroundColor: '#F94892',
              borderWidth: 2,
              borderColor: 'lightgray',
            }}
            title={'Sort'}
            onPress={() => setSortModalVisible(true)}></Button>
        </View>
        <View style={{width: 130}}>
          <Button
            buttonStyle={{
              backgroundColor: '#F94892',
              borderWidth: 2,
              borderColor: 'lightgray',
            }}
            title={'Layout'}
            onPress={() => {
              changeLayout();
            }}></Button>
        </View>
        <View style={{width: 130}}>
          <Button
            buttonStyle={{
              backgroundColor: '#F94892',
              borderWidth: 2,
              borderColor: 'lightgray',
            }}
            title={'Filter'}
            onPress={() => setFilterModalVisible(true)}></Button>
        </View>
      </View>

      <View style={styles.navAndAddButtons}>
        <View style={{width: 130, alignSelf: 'center', padding: 5}}>
          <Icon name="home" type="entypo" size={30} onPress={() => {}} />
        </View>
        <View style={{width: 130, alignSelf: 'center'}}>
          <Icon
            name="pluscircle"
            type="antdesign"
            size={30}
            onPress={() => setAddBookModalVisible(true)}
          />
        </View>
        <View style={{width: 130, alignSelf: 'center'}}>
          <Icon
            name="person"
            type="ionicons"
            size={35}
            onPress={() => navigation.navigate('Profile')}
          />
        </View>
      </View>

      {/* <View style={styles.logoutButton}>
        <Button
          title={'Logout'}
          onPress={() => {
            Logout();
          }}></Button>
      </View> */}

      <View style={{height: 50}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={sortModalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            setSortModalVisible(!sortModalVisible);
          }}>
          <View
            style={{
              height: '30%',
              marginTop: 'auto',
              backgroundColor: 'white',
              borderTopWidth: 2,
            }}>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'center',
                alignItems: 'center',
                marginTop: 5,
                marginRight: 5,
              }}>
              <View></View>
              <Text style={{marginLeft: 20}}>Sort on the basis of -</Text>
              <Icon
                name="circle-with-cross"
                type="entypo"
                size={30}
                onPress={() => setSortModalVisible(!sortModalVisible)}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Button
                title="Name"
                containerStyle={{
                  borderRadius: 30,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 5,
                }}
                onPress={() => {
                  bookDataList.sort((a, b) => {
                    let fa = a.name.toLowerCase(),
                      fb = b.name.toLowerCase();

                    if (fa < fb) {
                      return -1;
                    }
                    if (fa > fb) {
                      return 1;
                    }
                    return 0;
                  });
                }}
              />
              <Button
                title="Price"
                containerStyle={{
                  borderRadius: 30,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 5,
                }}
                onPress={() => {
                  bookDataList.sort((a, b) => {
                    return a.price - b.price;
                  });
                }}
              />
              <Button
                title="No. of Downloads"
                containerStyle={{
                  borderRadius: 30,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 5,
                }}
                onPress={() => {
                  bookDataList.sort((a, b) => {
                    return a.downloads - b.downloads;
                  });
                }}
              />
            </View>
          </View>
        </Modal>
      </View>

      <View style={{height: 50}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={filterModalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            setFilterModalVisible(!filterModalVisible);
          }}>
          <View
            style={{
              height: '40%',
              marginTop: 'auto',
              backgroundColor: 'white',
              borderTopWidth: 2,
            }}>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'center',
                alignItems: 'center',
                marginTop: 5,
                marginRight: 5,
              }}>
              <View></View>
              <Text style={{marginLeft: 20}}>Filter on the basis of - </Text>
              <Icon
                name="circle-with-cross"
                type="entypo"
                size={30}
                onPress={() => setFilterModalVisible(!filterModalVisible)}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Button
                title="Price Range less than 100"
                containerStyle={{
                  borderRadius: 30,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 5,
                }}
                onPress={() => {
                  // getBookDataList();
                  // setBookDataList(bookDataList);
                  const result = bookDataList.filter(obj => {
                    return obj.price < 100;
                  });
                  setBookDataList(result);
                }}
              />
              <Button
                title="Price Range between  100 and 500"
                containerStyle={{
                  borderRadius: 30,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 5,
                }}
                onPress={() => {
                  // await getBookDataList();
                  // setBookDataList(bookDataList);
                  const result = bookDataList.filter(obj => {
                    return 100 < obj.price < 500;
                  });
                  setBookDataList(result);
                }}
              />
              <Button
                title="Price Range greater than 500"
                containerStyle={{
                  borderRadius: 30,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 5,
                }}
                onPress={() => {
                  const result = bookDataList.filter(obj => {
                    return obj.price > 500;
                  });
                  setBookDataList(result);
                }}
              />
              <Button
                title="Currency Code"
                containerStyle={{
                  borderRadius: 30,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 5,
                }}
              />
            </View>
          </View>
        </Modal>
      </View>

      <View style={{height: 50}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={addBookModalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            setAddBookModalVisible(!addBookModalVisible);
            resetAddBookFormData();
          }}>
          <View
            style={{
              height: '70%',
              marginTop: 'auto',
              backgroundColor: 'white',
              borderTopWidth: 2,
            }}>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'center',
                alignItems: 'center',
                marginTop: 5,
                marginRight: 5,
              }}>
              <View></View>
              <Text
                style={{
                  marginLeft: 20,
                  textAlign: 'center',
                  width: 200,
                  fontWeight: 'bold',
                  fontSize: 15,
                }}>
                Enter the details of the Book{' '}
              </Text>
              <Icon
                name="circle-with-cross"
                type="entypo"
                size={30}
                onPress={() => {
                  setAddBookModalVisible(!addBookModalVisible),
                    resetAddBookFormData();
                }}
              />
            </View>
            <View
              style={{
                alignItems: 'center',
                marginTop: 30,
              }}>
              <View style={styles.inputView}>
                <TextInput
                  value={bookName}
                  style={styles.TextInput}
                  placeholder="Enter Book Name"
                  placeholderTextColor="#003f5c"
                  onChangeText={data => setBookName(data)}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  value={author}
                  style={styles.TextInput}
                  placeholder="Enter Author"
                  placeholderTextColor="#003f5c"
                  //   secureTextEntry={true}
                  onChangeText={data => setAuthor(data)}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  value={publisher}
                  style={styles.TextInput}
                  placeholder="Enter Publisher Name"
                  placeholderTextColor="#003f5c"
                  //   secureTextEntry={true}
                  onChangeText={data => setPublisher(data)}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  value={price}
                  style={styles.TextInput}
                  placeholder="Enter the Price of the Book"
                  placeholderTextColor="#003f5c"
                  //   secureTextEntry={true}
                  onChangeText={data => setPrice(data)}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  value={description}
                  style={styles.TextInput}
                  placeholder="Enter Desription of the Book"
                  placeholderTextColor="#003f5c"
                  //   secureTextEntry={true}
                  onChangeText={data => setDescription(data)}
                />
              </View>
              <Button
                title="Add Book"
                onPress={() => {
                  addBook();
                }}
                containerStyle={{
                  borderRadius: 20,
                  width: 200,
                  marginTop: 10,
                }}
                buttonStyle={{
                  backgroundColor: '#FF1493',
                }}
              />
            </View>
          </View>
        </Modal>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  mainWrapper: {
    backgroundColor: 'white',
    height: 590,
    width: '100%',
    justifyContent: 'space-around',
  },
  twoColumnLayoutWrapper: {
    display: 'flex',
    flexDirection: 'column',
  },
  oneColumnLayoutWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  logoutButton: {
    position: 'absolute',
    bottom: 2,
    width: '100%',
  },
  inputView: {
    backgroundColor: '#FFC0CB',
    borderRadius: 30,
    width: '70%',
    height: 45,
    marginBottom: 20,
    alignItems: 'flex-start',
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    textAlign: 'left',
    color: 'gray',
    // backgroundColor: 'orange',
  },

  bookNameStyle1: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  bookNameStyle2: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  priceStyle: {
    alignSelf: 'center',
    fontSize: 15,
    fontWeight: 'bold',
  },
  utilButtons: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 50,
    width: '100%',
    justifyContent: 'space-evenly',
    backgroundColor: 'white',
  },
  navAndAddButtons: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 5,
    width: '100%',
    justifyContent: 'space-evenly',
    backgroundColor: 'white',
    borderTopWidth: 3,
  },
  logo: {
    alignSelf: 'center',
    height: 70,
    width: 70,
  },
});
