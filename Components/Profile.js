import {View, Text, StyleSheet} from 'react-native';
import React, {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Button, Icon} from 'react-native-elements';

export default function Profile({navigation}) {
  const [userData, setUserData] = useState('');

  const getData = () => {
    try {
      AsyncStorage.getItem('UserData').then(value => {
        if (value != null) {
          setUserData(JSON.parse(value));
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const Logout = () => {
    navigation.navigate('Onboarding');
  };

  console.log('New 3 = ' + JSON.stringify(userData));

  return (
    <>
      <View>
        <Icon name="person" type="ionicons" size={100} onPress={() => {}} />
        <View
          style={{
            display: 'flex',
            alignItems: 'center',
          }}>
          <View
            style={{display: 'flex', alignItems: 'flex-start', marginTop: 20}}>
            <Text>
              <Text style={{fontWeight: 'bold'}}>Name:</Text> {userData.name}
            </Text>
            <Text>
              <Text style={{fontWeight: 'bold'}}>Date of Birth:</Text>{' '}
              {userData.dob}
            </Text>
            <Text>
              <Text style={{fontWeight: 'bold'}}>Gender:</Text>{' '}
              {userData.gender}
            </Text>
            <Text>
              <Text style={{fontWeight: 'bold'}}>Email:</Text> {userData.email}
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.logoutButton}>
        <Button
          title={'Logout'}
          onPress={() => {
            Logout();
          }}></Button>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  logoutButton: {
    // height: '100%',
    backgroundColor: 'yelllow',
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
});
