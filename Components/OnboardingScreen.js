import React from 'react';
import {View, StyleSheet, ImageBackground} from 'react-native';
import {Button, Text} from 'react-native-elements';

export default function OnboardingScreen({navigation}) {
  return (
    <>
      <ImageBackground
        source={require('../assets/Login.jpeg')}
        style={styles.background}>
        <View style={styles.container}>
          <View>
            <Text style={styles.textStyle}>Welcome to the Book Store</Text>
          </View>
          <View style={{position: 'relative', bottom: -160}}>
            <Button
              title="Continue"
              buttonStyle={{backgroundColor: '#F94892', borderRadius: 30}}
              containerStyle={{width: 100}}
              onPress={() => navigation.navigate('Login')}></Button>
          </View>
        </View>
      </ImageBackground>
    </>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 40,
    fontFamily: 'Poppins-Bold',
    color: 'white',
    textAlign: 'center',
  },
});
