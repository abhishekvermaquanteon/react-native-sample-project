import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function SignupScreen({navigation}) {
  const [name, setName] = useState('');
  const [dob, setDob] = useState('');
  const [gender, setGender] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const setData = async value => {
    if (
      password.length == 0 ||
      name.length == 0 ||
      dob.length == 0 ||
      gender.length == 0 ||
      email.length == 0 ||
      password.length == 0
    ) {
      Alert.alert('Please fill all the Information');
    } else {
      try {
        const jsonValue = JSON.stringify(value);
        console.log('New 1 = ' + jsonValue + ' ' + typeof jsonValue);
        await AsyncStorage.setItem('UserData', jsonValue);
        navigation.navigate('Login');
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView
        style={{
          width: '100%',
          backgroundColor: 'white',
          marginLeft: 0,
          height: '80%',
          marginTop: 50,
        }}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            alignContent: 'center',
          }}>
          <View style={styles.inputView}>
            <TextInput
              value={name}
              style={styles.TextInput}
              placeholder="Enter Your Name"
              placeholderTextColor="#003f5c"
              onChangeText={data => setName(data)}
            />
          </View>
          <View style={styles.inputView}>
            <TextInput
              value={dob}
              style={styles.TextInput}
              placeholder="D.O.B"
              placeholderTextColor="#003f5c"
              //   secureTextEntry={true}
              onChangeText={data => setDob(data)}
            />
          </View>
          <View style={styles.inputView}>
            <TextInput
              value={gender}
              style={styles.TextInput}
              placeholder="Gender"
              placeholderTextColor="#003f5c"
              //   secureTextEntry={true}
              onChangeText={data => setGender(data)}
            />
          </View>
          <View style={styles.inputView}>
            <TextInput
              value={email}
              style={styles.TextInput}
              placeholder="Email ID"
              placeholderTextColor="#003f5c"
              //   secureTextEntry={true}
              onChangeText={data => setEmail(data)}
            />
          </View>
          <View style={styles.inputView}>
            <TextInput
              value={password}
              style={styles.TextInput}
              placeholder="Create a Password"
              placeholderTextColor="#003f5c"
              //   secureTextEntry={true}
              onChangeText={data => setPassword(data)}
            />
          </View>
          <TouchableOpacity
            style={styles.loginBtn}
            onPress={() => {
              setData({
                name: name,
                dob: dob,
                gender: gender,
                email: email,
                password: password,
              });
            }}>
            <Text style={styles.loginText}>SIGNUP</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text style={styles.sign_up}>Login</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  image: {
    marginBottom: 40,
  },

  inputView: {
    backgroundColor: '#FFC0CB',
    borderRadius: 30,
    width: '70%',
    height: 45,
    marginBottom: 20,
    alignItems: 'flex-start',
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    textAlign: 'left',
  },

  forgot_button: {
    height: 30,
    marginBottom: 30,
  },

  sign_up: {
    height: 30,
    marginTop: 20,
  },

  loginBtn: {
    width: '80%',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
    backgroundColor: '#FF1493',
  },
  loginText: {
    color: 'white',
  },
});
