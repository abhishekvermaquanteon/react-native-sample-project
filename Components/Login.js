import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  Button,
  Image,
  TextInput,
} from 'react-native';

const Login = () => {
  return (
    <ImageBackground
      source={require('../assets/Login.jpeg')}
      style={styles.background}>
      <View style={styles.logoContainer}>
        <Image source={require('../assets/Logo.png')} style={styles.logo} />
        <Text style={styles.logoText}>Welcome to the Sample Project</Text>
      </View>
      <View style={styles.form}>
        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Name"
            placeholderTextColor="#003f5c"
            onChangeText={() => {}}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Date of Birth"
            placeholderTextColor="#003f5c"
            onChangeText={() => {}}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Gender"
            placeholderTextColor="#003f5c"
            onChangeText={() => {}}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Email"
            placeholderTextColor="#003f5c"
            onChangeText={() => {}}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Password"
            placeholderTextColor="#003f5c"
            onChangeText={() => {}}
          />
        </View>
      </View>
      <View title="Login" style={styles.loginButton}>
        <Button title="Hello" style={styles.loginButtonStyle}>
          LOGIN
        </Button>
      </View>
      <View title="Register" style={styles.registerButton}>
        <Text style={styles.loginButtonStyle}>REGISTER</Text>
      </View>
    </ImageBackground>
  );
};

// rnss shortcut for creating stylesheet below
const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  loginButton: {
    width: '100%',
    height: 40,
    backgroundColor: 'blue',
  },
  registerButton: {
    width: '100%',
    height: 40,
    backgroundColor: 'red',
    background: 'red',
    marginBottom: 10,
    marginTop: 10,
  },
  logo: {
    height: 100,
    width: 100,
    marginBottom: 10,
  },
  logoText: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white',
  },
  logoContainer: {
    position: 'absolute',
    top: 15,
    alignItems: 'center',
  },
  loginButtonStyle: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white',
    // backgroundColor: 'yellow',
  },
  inputView: {
    backgroundColor: 'white',
    borderRadius: 30,
    width: '70%',
    height: 45,
    marginBottom: 5,
    alignItems: 'center',
  },
  form: {
    position: 'absolute',
    top: 180,
    width: '100%',
    alignItems: 'center',
  },
});

export default Login;
